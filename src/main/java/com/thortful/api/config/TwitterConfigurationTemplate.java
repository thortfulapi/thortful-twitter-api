package com.thortful.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

import javax.inject.Inject;

/**
 * Created by Jeezy on 16/02/2017.
 */
@Configuration
public class TwitterConfigurationTemplate {


    @Inject
    private Environment env;



    private String consumerKey;


    private String consumerSecret;

    private String accessToken;


    private String accessTokenSecret;

    @Bean
    public TwitterTemplate twitterTemplate() {
         consumerKey = env.getProperty("twitter.consumerKey");
        consumerSecret = env.getProperty("twitter.consumerSecret");
        accessToken = env.getProperty("twitter.accessToken");
        accessTokenSecret = env.getProperty("twitter.accessTokenSecret");
        TwitterTemplate twitterOperations = new TwitterTemplate(consumerKey,
                consumerSecret, accessToken, accessTokenSecret);
        return twitterOperations;
    }
}
/**
 * Copyright (c) 2015 sothawo
 *
 * http://www.sothawo.com
 */
package com.thortful.api.rest;

import com.thortful.api.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/twitter")
public class TwitterRest {

    @Autowired
    private TwitterService twitterService;

    @RequestMapping(value = "/timeline/{twitterUser}" , method = RequestMethod.GET)
    public ResponseEntity<List<Tweet>> getUserTimeline(@PathVariable String twitterUser) {

        return new ResponseEntity(twitterService.getUserTimeline(twitterUser), HttpStatus.OK);
    }

    @RequestMapping(value = "/profile/{twitterUser}", method = RequestMethod.GET)
    public ResponseEntity getUserProfile(@PathVariable String twitterUser) {
        return new ResponseEntity(twitterService.getUserProfile(twitterUser), HttpStatus.OK);
    }


}

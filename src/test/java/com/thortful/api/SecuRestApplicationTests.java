package com.thortful.api;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecuRestApplicationTests {


	@Autowired
	private MockMvc mockMvc;

	@Test
	public void loginWithCorrectCredentials() throws Exception {
		mockMvc.perform(get("/twitter/timeline/j").with(user("user1").password("thortful"))
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
		)
				.andExpect(status().isOk());
	}

	@Test
	public void forGivenEmptyGetTimelineParameterThenError() throws Exception {
		mockMvc.perform(get("/twitter/timeline/").with(user("user1").password("thortful"))
				.contentType(MediaType.APPLICATION_FORM_URLENCODED))
				.andExpect(status().isNotFound());
	}

	@Test
	public void forGivenEmptyGetProfileParameterThenError() throws Exception {
		mockMvc.perform(get("/twitter/profile/").with(user("user1").password("thortful"))
				.contentType(MediaType.APPLICATION_FORM_URLENCODED))
				.andExpect(status().isNotFound());
	}

}
